# SMB - BI - Tools

# Goal: Tool to montior GA - Account Structure


## Primary goal of the management tool:

We wanted to have an overview over all accounts, properties and views for the complete SMB group.
Further we wanted to extract all the different settings like filters, goals, dimensions and metrics for the primary views.

Because of the huge maintenance effort to track all actions of the product and IT which might affect the google Analytics views or properties, we decided to build a small tool to extract the all needed informations from the google management API.

Result: Having a spreadsheet containing all the meta data of the accounts.

## Setup: 

* Clone the repository to your own workspace. Ideally do it with a sdk.
* Open terminal, go to the project directory, and execute: pip install -r requirements.txt or sudo pip install -r requirements.txt

* start with the first steps to integrate a sheets api: https://developers.google.com/sheets/api/quickstart/python
* The last step you have to rename the variable CLIENT_SECRET_KEY to the secret_key and move the secret key file to the folder.

* Acitvate google analytics API: In this step the project json and a service email are created ( use your own one if you have already done this)
* Move the downloaded project json to the folder of the tool, and edit in google_analytics.py the variable: KEY_FILE_ANALYTICS to the name of the file.

* In this step you also got a service mail, rename the variable SERVICE_EMAIL to your email. 
* Add the service email as a normal user to your analytics account
* Activate the sheets api as well, like https://developers.google.com/sheets/api/quickstart/python

* create spreadsheet like https://docs.google.com/spreadsheets/d/1g92PXPgHWFInEzJP0qJ21pk6RsR_vJ-IbFjaViHwPeA/edit?usp=sharing

* Run the code.


## Notes:

There are still some points open which might be done with the API: https://developers.google.com/analytics/devguides/config/mgmt/v3/

A nice feature would be to manage users with the API or at least retreive all the users of all accounts/properties/views. This however requires to have "Manage Users" rights for the service account.