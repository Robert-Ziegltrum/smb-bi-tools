'''
Created on 01.11.2017

@author: robertziegltrum
'''
import argparse

from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

import httplib2
from oauth2client import client
from oauth2client import file
from oauth2client import tools
import csv
import json
from datetime import datetime 

import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage


CREDENTIALS_PATH = 'sheets.googleapis.com-python-quickstart.json'
TARGET_SPREADSHEET = '1zgajKfno1LjMdDnxtc_VXX9SCxKXcg1crDtf9rPWMJk'
KEY_FILE_ANALYTICS = './bi_analytics_js.json'
SERVICE_EMAIL = 'analytics-test@bi-toolbox.iam.gserviceaccount.com'
CLIENT_SECRET_FILE = 'client_secret_43254796196-7fga0n82aepbs8he9qh9gpjkvho4bjlu.apps.googleusercontent.com.json'


def get_service(api_name, api_version, scope, key_file_location, user_email):
	"""Get a service that communicates to a Google API.

	Args:
		api_name: The name of the api to connect to.
		api_version: The api version to connect to.
		scope: A list auth scopes to authorize for the application.
		key_file_location: The path to a valid service account JSON key file.

	Returns:
		A service that is connected to the specified API.
	"""

	credentials = ServiceAccountCredentials.from_json_keyfile_name(key_file_location, scopes=scope)
	service = build(api_name, api_version, credentials=credentials)
	return service


def get_account_info(service):
	""" Use the Analytics service object to get the first profile id.

	Get a list of all Google Analytics accounts for this user
	"""
	accounts = service.management().accounts().list().execute()
	property_data = []
	account_data = []
	account_data.append( ['name','created','id'])
	property_data.append(['account_name','id','property_name','websiteUrl','level','industryVertical','profilCount','created'])
	profil_data = []
	profil_data.append(['account_name','property_name','id','view_name','currency','timezone','type','created'])
	try:
		for elem in accounts.get('items'):
			account_item=[]
			account_item.append(elem.get('name'))
			account_item.append(elem.get('created').split('T')[0])
			account_id = elem.get('id')
			account_item.append(account_id)
			props = service.management().webproperties().list(accountId=account_id).execute()
			for item in props.get('items'):
				property_item = []
				property_item.append(account_item[0])
				property_id = item.get('id')
				property_item.append(property_id)
				property_item.append(item.get('name'))
				property_item.append(item.get('websiteUrl'))
				property_item.append(item.get('level'))
				property_item.append(item.get('industryVertical'))
				property_item.append(item.get('profileCount'))
				property_item.append(item.get('created').split('T')[0])
				property_data.append(property_item)
				
				profiles = service.management().profiles().list(
						accountId=account_id,
						webPropertyId= property_id ).execute()
				for elems in profiles.get('items'):
					profil_item = []
					profil_item.append(account_item[0])
					profil_item.append(property_item[2])
					profil_item.append(elems.get('id'))
					profil_item.append(elems.get('name'))
					profil_item.append(elems.get('currency'))
					profil_item.append(elems.get('timezone'))
					profil_item.append(elems.get('type'))
					profil_item.append(elems.get('created').split('T')[0])
					profil_data.append(profil_item)
			
			account_data.append(account_item)
	except:
		pass

	data = {}
	data['values'] = profil_data
	set_data(value_range_body=data, data_type='Views')
	data = {}
	data['values'] = account_data
	set_data(value_range_body=data, data_type='Accounts')
	data = {}
	data['values'] = property_data
	set_data(value_range_body=data, data_type='Properties')


def get_credentials():
	"""Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
	
	SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
	APPLICATION_NAME = 'Google Sheets API'
	flags = None
	credential_path = os.path.join( os.getcwd(), CREDENTIALS_PATH)
	store = Storage(credential_path)
	credentials = store.get()
	if not credentials or credentials.invalid:
		flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
		flow.user_agent = APPLICATION_NAME	
		flags = tools.argparser.parse_args([]) 	
		credentials = tools.run_flow(flow, store, flags)
	return credentials


def set_data( value_range_body, data_type):	
	rageName='%s!A1:R1000' % data_type
	credentials = get_credentials()
	http = credentials.authorize(httplib2.Http())
	discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
	service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)
	

	request = service.spreadsheets().values().update(spreadsheetId=TARGET_SPREADSHEET,valueInputOption='USER_ENTERED',
                                            range=rageName, body=value_range_body)
	response = request.execute()
	
	
def get_filters(service):
	accounts = service.management().accounts().list().execute()
	filter_items = []
	header = []
	for col in ('account_name','account_id','filter_name','filter_type','created','updated','fields','match_type','expression','case_sensitive'):
		header.append(col)
	filter_items.append(header)
	
	for elem in accounts.get('items'):
		filter_list = service.management().filters().list(accountId=elem.get('id')).execute()
		for filters in filter_list.get('items'):
			filter_item= []
			filter_item.append(elem.get('name'))
			filter_item.append(elem.get('id'))
			filter_item.append(filters.get('name'))
			filter_item.append(filters.get('type'))
			filter_item.append(filters.get('created').split('T')[0])
			filter_item.append(filters.get('updated').split('T')[0])
			if filters.get('includeDetails'):
				filter_item.append(filters.get('includeDetails').get('field'))
				filter_item.append(filters.get('includeDetails').get('matchType'))
				filter_item.append(filters.get('includeDetails').get('expressionValue'))
				filter_item.append(filters.get('includeDetails').get('caseSensitive'))
			if filters.get('excludeDetails'):
				filter_item.append(filters.get('excludeDetails').get('field'))
				filter_item.append(filters.get('excludeDetails').get('matchType'))
				filter_item.append(filters.get('excludeDetails').get('expressionValue'))
				filter_item.append(filters.get('excludeDetails').get('caseSensitive'))
			filter_items.append(filter_item)
	data = {}
	data['values'] = filter_items
	set_data(value_range_body=data, data_type='Filters')	

	
def get_custom_metrics(service):
	custom_metrics_data = []
	header = []
	for col in ('account_name','property_name','custom_metrics_id','name','index','scope','active','type','created','updated'):
		header.append(col)
	custom_metrics_data.append(header)
	accounts = service.management().accounts().list().execute()
	for account in accounts.get('items'):
		props = service.management().webproperties().list(accountId=account.get('id')).execute()
		for property in props.get('items'):
			result = service.management().customMetrics().list(accountId=account.get('id'),webPropertyId=property.get('id')).execute()
			if result.get('totalResults') > 0:
				for elem in result.get('items'):
					metrics = []
					metrics.append(account.get('name'))
					metrics.append(property.get('name'))
					metrics.append(elem.get('id'))
					metrics.append(elem.get('name'))
					metrics.append(elem.get('index'))
					metrics.append(elem.get('scope'))
					metrics.append(elem.get('active'))
					metrics.append(elem.get('type'))
					metrics.append(elem.get('created').split('T')[0])
					metrics.append(elem.get('updated').split('T')[0])
					custom_metrics_data.append(metrics)
		
	data = {}
	data['values'] = custom_metrics_data
	set_data(value_range_body=data, data_type='Custom-Metrics')	

def get_custom_dimensions(service):
	custom_metrics_data = []
	header = []
	for col in ('account_name','property_name','custom_dimenison_id','name','index','scope','active','type','created','updated'):
		header.append(col)
	custom_metrics_data.append(header)
	accounts = service.management().accounts().list().execute()
	for account in accounts.get('items'):
		props = service.management().webproperties().list(accountId=account.get('id')).execute()
		for property in props.get('items'):
			result = service.management().customDimensions().list(accountId=account.get('id'),webPropertyId=property.get('id')).execute()
			if result.get('totalResults') > 0:				
				for elem in result.get('items'):
					metrics = []
					metrics.append(account.get('name'))
					metrics.append(property.get('name'))
					metrics.append(elem.get('id'))
					metrics.append(elem.get('name'))
					metrics.append(elem.get('index'))
					metrics.append(elem.get('scope'))
					metrics.append(elem.get('active'))
					metrics.append(elem.get('type'))
					metrics.append(elem.get('created').split('T')[0])
					metrics.append(elem.get('updated').split('T')[0])
					custom_metrics_data.append(metrics)
					
	data = {}
	data['values'] = custom_metrics_data
	set_data(value_range_body=data, data_type='Custom-Dimensions')

def get_goals(service):
	custom_metrics_data = []
	header = []
	for col in ('account_name','property_name','view-name','goal_id','name','value','active','type','created','updated'):
		header.append(col)
	custom_metrics_data.append(header)
	
	accounts = service.management().accounts().list().execute()
	for account in accounts.get('items'):
		props = service.management().webproperties().list(accountId=account.get('id')).execute()
		for property in props.get('items'):
			profiles = service.management().profiles().list(accountId=account.get('id'),webPropertyId= property.get('id')).execute()
			for profile in profiles.get('items'):
				result = service.management().goals().list(accountId=account.get('id'),webPropertyId=property.get('id'),profileId=profile.get('id')).execute()
				if(result.get('totalResults')) > 0:
					for elem in result.get('items'):
						metrics = []
						metrics.append(account.get('name'))
						metrics.append(property.get('name'))
						metrics.append(profile.get('name'))
						metrics.append(elem.get('id'))
						metrics.append(elem.get('name'))
						metrics.append(elem.get('value'))
						metrics.append(elem.get('active'))
						metrics.append(elem.get('type'))
						metrics.append(elem.get('created').split('T')[0])
						metrics.append(elem.get('updated').split('T')[0])
						custom_metrics_data.append(metrics)
	data = {}
	data['values'] = custom_metrics_data
	set_data(value_range_body=data, data_type='Goals')

def set_current_timestamp_to_sheet():
	data = {}
	data_array=[]
	data_array.append( ['The last run of the report was', '%s' % str(datetime.now()) ])
	data['values'] = data_array
	set_data(value_range_body=data, data_type='Last Run')

def main():
	# Define the auth scopes to request.
	scope = ['https://www.googleapis.com/auth/analytics.readonly']
	
	# Authenticate and construct service.
	service = get_service('analytics', 'v3', scope, KEY_FILE_ANALYTICS, SERVICE_EMAIL)
	
	set_current_timestamp_to_sheet()
	get_account_info(service)
	get_filters(service)
	get_custom_metrics(service)
	get_goals(service)
	get_custom_dimensions(service)


if __name__ == '__main__':
	main()
